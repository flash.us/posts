package ru.idyachenko.posts;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PostsApplicationTests {
	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	void contextLoads() {}

	@Test
	public void testUploadAndRetrievePicture() {
		// Create user
		String createPictureUrl = "http://localhost:" + port + "/users";

		// User vasya = new User("Vasya", "Petrov", "Ivanovich", "http://", "vasya",
		// "vasya@mail.com");
		// vasya.setGender(Gender.MALE);
		// ResponseEntity<String> responseEntity =
		// restTemplate.postForEntity(createUserUrl, vasya, String.class);
		// assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
		// String createdUserId = responseEntity.getHeaders().get("X-UserId").get(0);
		// vasya.setId(UUID.fromString(createdUserId));

		// // Retrieve user
		// var getResponse =
		// restTemplate.getForEntity(createUserUrl + "/" + createdUserId, User.class);
		// assertEquals(HttpStatus.OK, getResponse.getStatusCode());
		// assertEquals(vasya, getResponse.getBody());

	}

}
