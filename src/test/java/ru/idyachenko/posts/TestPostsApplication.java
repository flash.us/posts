package ru.idyachenko.posts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.TestConfiguration;

@TestConfiguration(proxyBeanMethods = false)
public class TestPostsApplication {

	public static void main(String[] args) {
		SpringApplication.from(PostsApplication::main).with(TestPostsApplication.class).run(args);
	}

}
