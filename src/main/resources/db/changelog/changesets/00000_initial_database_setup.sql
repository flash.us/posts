-- create database posts;
-- drop table posts, pictures, users cascade;

CREATE TABLE "posts" (
  "id" UUID PRIMARY KEY,
  "user_id" UUID NOT NULL,
  "title" varchar NOT NULL,
  "body" text NOT NULL,
  "status" varchar NOT NULL,
  "created_at" timestamp NOT NULL
);

CREATE TABLE "pictures" (
  "id" UUID PRIMARY KEY,
  "title" varchar NOT NULL,
  "url" varchar NOT NULL,
  "post_id" UUID NOT NULL,
  "created_at" timestamp NOT NULL
);

CREATE INDEX IF NOT EXISTS "i_user_id" ON "posts" USING BTREE ("user_id");

CREATE INDEX IF NOT EXISTS "i_title" ON "posts" USING BTREE ("title");

CREATE INDEX IF NOT EXISTS "i_created_at" ON "posts" USING BTREE ("created_at");

CREATE INDEX IF NOT EXISTS "i_post_id" ON "pictures" USING HASH ("post_id");

CREATE INDEX IF NOT EXISTS "i_created_at" ON "pictures" USING BTREE ("created_at");

ALTER TABLE "pictures" ADD FOREIGN KEY ("post_id") REFERENCES "posts" ("id")
ON DELETE restrict;
