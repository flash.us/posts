package ru.idyachenko.posts.repository;

import com.amazonaws.services.s3.AmazonS3;
import ru.idyachenko.posts.properties.S3Properties;
import org.springframework.stereotype.Component;

@Component
public class PicturesRepository extends S3Repository {
    public PicturesRepository(AmazonS3 s3Client, S3Properties properties) {
        super(s3Client, properties.getBucket());
    }
}
