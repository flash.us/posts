package ru.idyachenko.posts;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;
// import org.apache.el.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.info.BuildProperties;

import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import ru.idyachenko.posts.repository.PicturesRepository;
import ru.idyachenko.posts.repository.S3Repository;
import ru.idyachenko.posts.service.StorageService;

@SpringBootApplication
public class PostsApplication {
	@Autowired
	BuildProperties buildProperties;

	@Autowired
	private Environment env;

	public static void main(String[] args) {
		SpringApplication.run(PostsApplication.class, args);
	}

	@Bean
	CommandLineRunner demoJpa(PicturesRepository picturesRepository, S3Repository s3Repository) {
		return (args) -> {

			// // check folder contents

			// Path rootLocation = Paths.get("src\\main\\resources\\static\\");
			// try {
			// Stream<Path> paths = Files.walk(rootLocation, 1)
			// .filter(path -> !path.equals(rootLocation)).map(rootLocation::relativize);
			// paths.forEach(System.out::println);
			// paths.close();
			// System.out.println("Number of elements: " + paths.count());
			// System.out.println(
			// "Number of directories: " + paths.filter(Files::isDirectory).count());
			// System.out
			// .println("Number of files: " + paths.filter(Files::isRegularFile).count());
			// System.out.println(
			// "Number of symbolic links: " + paths.filter(Files::isSymbolicLink).count());
			// } catch (IOException e) {
			// e.printStackTrace();
			// }

			// store picture in S3 bucket using StorageService
			String key = "ruble_ico";
			StorageService storageService = new StorageService(picturesRepository);
			storageService.putPictureFile(key, new File("src\\main\\resources\\static\\ruble.ico"));
			System.out.println("Picture " + key + " stored in S3 bucket");

			// get picture from S3 bucket using StorageService
			storageService.getPictureFileContent(key);
			System.out.println("Picture " + key + " retrieved from S3 bucket");

			// delete picture from S3 bucket using StorageService
			storageService.deletePictureFile(key);
			System.out.println("Picture " + key + " deleted from S3 bucket");

			System.out.println(
					"====================================================================================");
			System.out.println(
					"====================================================================================");
			System.out.println("Application properties:");

			System.out.println("Name: " + buildProperties.getName());
			System.out.println("Version: " + buildProperties.getVersion());
			System.out.println("Build Time: " + buildProperties.getTime());
			System.out.println("Artifact: " + buildProperties.getArtifact());
			System.out.println("Group: " + buildProperties.getGroup());

			System.out.println("Port: " + env.getProperty("server.port"));
			System.out.println(
					"====================================================================================");

		};
	}
}
