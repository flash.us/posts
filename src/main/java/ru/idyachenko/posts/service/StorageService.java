package ru.idyachenko.posts.service;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import lombok.RequiredArgsConstructor;
import ru.idyachenko.posts.repository.PicturesRepository;
import ru.idyachenko.posts.repository.S3Repository;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collection;
import java.util.Date;

@Service
@RequiredArgsConstructor
public class StorageService {
    private final PicturesRepository picRepository;

    public Collection<String> getAllPictureFiles() {
        return picRepository.listKeys("/");
    }

    public InputStream getPictureFileContent(String key) {
        return picRepository.get(key).map(S3Object::getObjectContent)
                .orElseThrow(() -> new IllegalStateException("Object not found " + key));
    }

    public LocalDateTime getPictureFileModificationTime(String key) {
        return getObjectLastModified(picRepository, key);
    }

    public void putPictureFile(String key, File file) throws IOException {
        try (var stream = new FileInputStream(file)) {
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentLength(file.length());
            metadata.setContentType(MediaType.TEXT_HTML_VALUE);
            picRepository.put(key, stream, metadata);
        }
    }

    public void deletePictureFile(String key) {
        picRepository.delete(key);
    }

    private LocalDateTime getObjectLastModified(S3Repository repository, String key) {
        return repository.get(key).map(S3Object::getObjectMetadata)
                .map(ObjectMetadata::getLastModified).map(this::dateToLocal)
                .orElse(LocalDateTime.MIN);
    }

    private LocalDateTime dateToLocal(Date date) {
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }
}
